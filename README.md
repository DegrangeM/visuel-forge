# Kit graphique de La Forge

Bienvenue sur la forge des communs numériques éducatifs de **Brigit** et **Komit** !

![Travail graphique de Juliette Taka](https://forge.apps.education.fr/framaka/visuel-forge/-/raw/main/juliette-taka/la_forge_juliette_taka_0_intro.png "Visuel de La Forge")

Les visuels de La Forge ont été réalisés en septembre 2023 par [Juliette Taka](https://juliettetaka.com/) et sont sous licence libre Creative Commons [CC-BY](https://creativecommons.org/licenses/by/4.0/deed.fr).

Juliette Taka est illustratrice et sketchnoteuse freelance. Elle [contribue aux logiciels libres](https://juliettetaka.com/fr/linux) en ayant notamment créé le thème par défaut de plusieurs versions de la distribution GNU/Linux Debian. Vous pouvez la suivre sur [Mastodon](https://social.logilab.org/@julietteTaka) ou [X](https://twitter.com/JulietteTaka).

## Logo

![Travail graphique de Juliette Taka](https://forge.apps.education.fr/framaka/visuel-forge/-/raw/main/juliette-taka/la_forge_juliette_taka_1_logo.png "Visuel de La Forge - Logo")

Le logo de la Forge des communs numériques éducatifs rappelle celui de [Git](https://fr.wikipedia.org/wiki/Git).

Il est propsé en deux versions, large et compacte, et en deux formats, png et svg.

## Brigit & Komit

![Travail graphique de Juliette Taka](https://forge.apps.education.fr/framaka/visuel-forge/-/raw/main/juliette-taka/la_forge_juliette_taka_2_image_finale.png "Visuel de La Forge - Brigit & Komit")

Dans la mythologie celtique, la déesse [Brigit](https://fr.wikipedia.org/wiki/Brigit) « est associée à la saison printanière, à la fertilité, à la médecine, à la poésie et aux arts dont ceux de la forge ». Sur le tablier de Brigit, on peut lire _101010_ mais nous ignorons à ce jour sa signification.

La castor Komit est l'ami de Brigit et l'animal-totem de la forge. La castor est surnommé _l'ingénieur des écosystèmes_ ou _l'architecte de nos rivières_ par sa capacité à modifier son environnement en construisant collaborativement des barrages utiles à sa communauté.

Le style BD-manga nous rappelle que nous sommes dans l'éducation.

![Travail graphique de Juliette Taka](https://forge.apps.education.fr/framaka/visuel-forge/-/raw/main/juliette-taka/la_forge_juliette_taka_3_personnages.png "Visuel de La Forge - Personnages")

Nous invitons les utilisatrices et utilisateurs de la forge à s'emparer de ces visuels comme bon leur semble pour signaler que le code ou contenu de leurs projets sont sur la forge. Ils peuvent egalement s'intégrer dans les slides de vos présentations.

On peut télécharger l'ensemble du visuel en [version light](https://forge.apps.education.fr/framaka/visuel-forge/-/raw/main/Graphique_La_Forge_Kit_Projet.zip) (2 Mo) ou en [version complète](https://forge.apps.education.fr/framaka/visuel-forge/-/raw/main/Graphique_La_Forge_Kit_Complet.zip?ref_type=heads) (57 Mo).

![Travail graphique de Juliette Taka](https://forge.apps.education.fr/framaka/visuel-forge/-/raw/main/juliette-taka/la_forge_juliette_taka_6_reseaux_sociaux.png "Visuel de La Forge - Réseaux Sociaux")

_À noter aussi [la mascotte Komit (et ses dérivées)](https://forge.apps.education.fr/lium/mascotte-castor-forge) du GTNum Forges. Et [l'écureuil forgeron](https://www.peppercarrot.com/en/viewer/misc-src__2022-11-27_Forgejo_by-David-Revoy.html) de David Revoy réalisé pour le projet de forge libre Forgejo._






